there are two dump files in this directory:

structure.sql is ALL tables but only their table structure, no data.
include stored procedures/functions/events/triggers/create schema

data.sql is ALL table data for static game objects and contains NO server variables or player data
do NOT include stored procedures/functions/events/triggers/create schema
currently the exclusions are
account_exceptions
account_ip_record
accounts
accounts_banned
accounts_parties
accounts_sessions
auction_house
audit_chat
audit_gm
char_blacklist
char_effects
char_equip
char_exp
char_inventory
char_job_sets
char_jobs
char_look
char_merit
char_pet
char_points
char_profile
char_recast
char_skills
char_spells
char_stats
char_storage
char_style
char_unlocks
char_vars
chars
cheat_incidents
crash_reports
delivery_box
flist
flist_settings
linkshells
server_variables
web_tokens

for all dumps, enable hex-blobs and disable extended-insert and disable column statistics (MYSQL)
export to self-contained file

if setting up a new server branched off this repo, simply run structure.sql and then data.sql