CREATE DATABASE  IF NOT EXISTS `ffxiwings` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ffxiwings`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ffxiwings
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abilities`
--

DROP TABLE IF EXISTS `abilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `abilities` (
  `abilityId` smallint(5) unsigned NOT NULL,
  `name` tinytext DEFAULT NULL,
  `job` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `level` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `validTarget` smallint(3) unsigned NOT NULL DEFAULT 0,
  `recastTime` smallint(5) unsigned NOT NULL DEFAULT 0,
  `recastId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `message1` smallint(5) unsigned NOT NULL DEFAULT 0,
  `message2` smallint(5) unsigned NOT NULL DEFAULT 0,
  `animation` smallint(5) unsigned NOT NULL DEFAULT 0,
  `animationTime` smallint(4) unsigned NOT NULL DEFAULT 0,
  `castTime` smallint(4) unsigned NOT NULL DEFAULT 0,
  `actionType` tinyint(2) unsigned NOT NULL DEFAULT 6,
  `range` float(3,1) unsigned NOT NULL DEFAULT 0.0,
  `isAOE` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `CE` smallint(5) NOT NULL DEFAULT 0,
  `VE` smallint(5) NOT NULL DEFAULT 0,
  `meritModID` smallint(4) NOT NULL DEFAULT 0,
  `addType` smallint(2) NOT NULL DEFAULT 0,
  `content_tag` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`abilityId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=56;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `abilities_charges`
--

DROP TABLE IF EXISTS `abilities_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `abilities_charges` (
  `recastId` smallint(5) unsigned NOT NULL,
  `job` tinyint(2) unsigned NOT NULL,
  `level` tinyint(2) unsigned NOT NULL,
  `maxCharges` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `chargeTime` smallint(4) unsigned NOT NULL DEFAULT 0,
  `meritModID` smallint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`recastId`,`job`,`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=56;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_exceptions`
--

DROP TABLE IF EXISTS `account_exceptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_exceptions` (
  `accide` int(10) NOT NULL,
  PRIMARY KEY (`accide`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_ip_record`
--

DROP TABLE IF EXISTS `account_ip_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_ip_record` (
  `login_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `accid` int(10) NOT NULL,
  `charid` int(10) DEFAULT NULL,
  `client_ip` tinytext NOT NULL,
  PRIMARY KEY (`login_time`,`accid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `login` varchar(16) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `current_email` varchar(64) NOT NULL DEFAULT '',
  `registration_email` varchar(64) NOT NULL DEFAULT '',
  `timecreate` datetime NOT NULL DEFAULT current_timestamp(),
  `timelastmodify` timestamp NOT NULL DEFAULT current_timestamp(),
  `content_ids` tinyint(2) unsigned NOT NULL DEFAULT 3,
  `expansions` smallint(4) unsigned NOT NULL DEFAULT 30,
  `features` tinyint(2) unsigned NOT NULL DEFAULT 12,
  `status` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `priv` tinyint(3) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER account_delete
    BEFORE DELETE ON accounts
    FOR EACH ROW
BEGIN
    DELETE FROM `accounts_banned` WHERE `accid` = OLD.id;
    DELETE FROM `chars` WHERE `accid` = OLD.id;     
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `accounts_banned`
--

DROP TABLE IF EXISTS `accounts_banned`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accounts_banned` (
  `accid` int(10) unsigned NOT NULL DEFAULT 0,
  `timebann` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `timeunbann` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `banncomment` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`accid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accounts_parties`
--

DROP TABLE IF EXISTS `accounts_parties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accounts_parties` (
  `charid` int(10) unsigned NOT NULL DEFAULT 0,
  `partyid` int(10) unsigned NOT NULL DEFAULT 0,
  `partyflag` smallint(5) unsigned NOT NULL DEFAULT 0,
  `allianceid` int(10) unsigned NOT NULL DEFAULT 0,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`charid`),
  CONSTRAINT `accounts_parties_ibfk_1` FOREIGN KEY (`charid`) REFERENCES `accounts_sessions` (`charid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accounts_sessions`
--

DROP TABLE IF EXISTS `accounts_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accounts_sessions` (
  `accid` int(10) unsigned NOT NULL DEFAULT 0,
  `charid` int(10) unsigned NOT NULL DEFAULT 0,
  `targid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `linkshellid1` int(10) unsigned NOT NULL DEFAULT 0,
  `linkshellrank1` smallint(5) unsigned NOT NULL DEFAULT 0,
  `linkshellid2` int(10) unsigned NOT NULL DEFAULT 0,
  `linkshellrank2` smallint(5) unsigned NOT NULL DEFAULT 0,
  `session_key` binary(20) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `server_addr` int(10) unsigned zerofill NOT NULL DEFAULT 0000000000,
  `server_port` smallint(5) unsigned NOT NULL DEFAULT 0,
  `client_addr` int(10) unsigned zerofill NOT NULL DEFAULT 0000000000,
  `client_port` smallint(5) unsigned NOT NULL DEFAULT 0,
  `version_mismatch` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `FLsentnotif` tinyint(2) unsigned DEFAULT 0,
  `client_version` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`charid`),
  UNIQUE KEY `accid` (`accid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER session_delete
    BEFORE DELETE ON accounts_sessions
    FOR EACH ROW
BEGIN
    UPDATE `char_stats` SET zoning = 0 WHERE `charid` = OLD.charid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `auction_house`
--

DROP TABLE IF EXISTS `auction_house`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auction_house` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `stack` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `seller` int(10) unsigned NOT NULL DEFAULT 0,
  `seller_name` varchar(15) DEFAULT NULL,
  `date` int(10) unsigned NOT NULL DEFAULT 0,
  `price` int(10) unsigned NOT NULL DEFAULT 0,
  `buyer_name` varchar(15) DEFAULT NULL,
  `sale` int(10) unsigned NOT NULL DEFAULT 0,
  `sell_date` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `itemid` (`itemid`),
  KEY `charid` (`seller`)
) ENGINE=InnoDB AUTO_INCREMENT=161801 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auction_house_buy
    BEFORE UPDATE ON auction_house
    FOR EACH ROW
BEGIN
    IF OLD.seller != 0 AND NEW.sale != 0 THEN INSERT INTO delivery_box VALUES (NEW.seller, NEW.seller_name, 1, 0, 0xFFFF, NEW.itemid, NEW.sale, NULL, 0, 'AH-Jeuno', 0, 0); END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `audit_chat`
--

DROP TABLE IF EXISTS `audit_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_chat` (
  `lineID` int(10) NOT NULL AUTO_INCREMENT,
  `speaker` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `lsName` tinytext DEFAULT NULL,
  `recipient` tinytext DEFAULT NULL,
  `message` blob DEFAULT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`lineID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audit_gm`
--

DROP TABLE IF EXISTS `audit_gm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_gm` (
  `date_time` datetime NOT NULL,
  `gm_name` varchar(16) NOT NULL,
  `command` varchar(40) NOT NULL,
  `full_string` varchar(200) NOT NULL,
  PRIMARY KEY (`date_time`,`gm_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `augments`
--

DROP TABLE IF EXISTS `augments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `augments` (
  `augmentId` smallint(5) unsigned NOT NULL,
  `multiplier` smallint(2) NOT NULL DEFAULT 0,
  `modId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `value` smallint(5) NOT NULL DEFAULT 0,
  `isPet` tinyint(1) NOT NULL DEFAULT 0,
  `petType` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`augmentId`,`multiplier`,`modId`,`isPet`,`petType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `automaton_spells`
--

DROP TABLE IF EXISTS `automaton_spells`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `automaton_spells` (
  `spellid` smallint(4) unsigned NOT NULL,
  `skilllevel` smallint(3) unsigned NOT NULL DEFAULT 0,
  `heads` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `enfeeble` smallint(4) unsigned NOT NULL DEFAULT 0,
  `immunity` smallint(4) unsigned NOT NULL DEFAULT 0,
  `removes` int(6) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`spellid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=14;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bcnm_battlefield`
--

DROP TABLE IF EXISTS `bcnm_battlefield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bcnm_battlefield` (
  `bcnmId` smallint(5) unsigned NOT NULL,
  `battlefieldNumber` tinyint(3) DEFAULT NULL,
  `monsterId` int(10) NOT NULL,
  `conditions` tinyint(2) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=56;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bcnm_info`
--

DROP TABLE IF EXISTS `bcnm_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bcnm_info` (
  `bcnmId` smallint(5) unsigned NOT NULL,
  `zoneId` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `name` varchar(30) NOT NULL,
  `fastestName` varchar(15) DEFAULT 'Not Set!',
  `fastestPartySize` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `fastestTime` int(10) unsigned DEFAULT 1,
  `timeLimit` smallint(5) unsigned NOT NULL DEFAULT 1800,
  `levelCap` smallint(5) unsigned NOT NULL DEFAULT 75,
  `partySize` smallint(5) unsigned NOT NULL DEFAULT 6,
  `lootDropId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `rules` smallint(5) unsigned NOT NULL DEFAULT 0,
  `isMission` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`bcnmId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=56;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bcnm_treasure_chests`
--

DROP TABLE IF EXISTS `bcnm_treasure_chests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bcnm_treasure_chests` (
  `bcnmId` smallint(5) unsigned NOT NULL,
  `battlefieldNumber` tinyint(3) DEFAULT NULL,
  `npcId` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=56;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blue_spell_list`
--

DROP TABLE IF EXISTS `blue_spell_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blue_spell_list` (
  `spellid` smallint(3) NOT NULL,
  `mob_skill_id` smallint(4) unsigned NOT NULL,
  `set_points` smallint(2) NOT NULL,
  `trait_category` smallint(2) NOT NULL,
  `trait_category_weight` smallint(2) NOT NULL,
  `primary_sc` smallint(2) NOT NULL,
  `secondary_sc` smallint(2) NOT NULL,
  PRIMARY KEY (`spellid`,`mob_skill_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blue_spell_mods`
--

DROP TABLE IF EXISTS `blue_spell_mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blue_spell_mods` (
  `spellId` smallint(3) unsigned NOT NULL,
  `modid` smallint(5) unsigned NOT NULL,
  `value` smallint(5) NOT NULL DEFAULT 0,
  PRIMARY KEY (`spellId`,`modid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blue_traits`
--

DROP TABLE IF EXISTS `blue_traits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blue_traits` (
  `trait_category` smallint(2) unsigned NOT NULL,
  `trait_points_needed` smallint(2) unsigned NOT NULL,
  `traitid` tinyint(3) unsigned NOT NULL,
  `modifier` smallint(5) unsigned NOT NULL,
  `value` smallint(5) NOT NULL,
  PRIMARY KEY (`trait_category`,`trait_points_needed`,`modifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_blacklist`
--

DROP TABLE IF EXISTS `char_blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_blacklist` (
  `charid_owner` int(10) unsigned NOT NULL,
  `charid_target` int(10) unsigned NOT NULL,
  PRIMARY KEY (`charid_target`,`charid_owner`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_effects`
--

DROP TABLE IF EXISTS `char_effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_effects` (
  `charid` int(10) unsigned NOT NULL,
  `effectid` smallint(5) unsigned NOT NULL,
  `icon` smallint(5) unsigned NOT NULL DEFAULT 0,
  `power` smallint(5) unsigned NOT NULL DEFAULT 0,
  `tick` int(10) unsigned NOT NULL DEFAULT 0,
  `duration` int(10) unsigned NOT NULL DEFAULT 0,
  `subid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `subpower` smallint(5) NOT NULL DEFAULT 0,
  `tier` smallint(5) unsigned NOT NULL DEFAULT 0,
  `flags` int(8) unsigned NOT NULL DEFAULT 0,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `charid` (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_equip`
--

DROP TABLE IF EXISTS `char_equip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_equip` (
  `charid` int(10) unsigned NOT NULL,
  `slotid` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `equipslotid` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `containerid` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`,`equipslotid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=41;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_exp`
--

DROP TABLE IF EXISTS `char_exp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_exp` (
  `charid` int(10) unsigned NOT NULL,
  `mode` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `war` smallint(5) unsigned NOT NULL DEFAULT 0,
  `mnk` smallint(5) unsigned NOT NULL DEFAULT 0,
  `whm` smallint(5) unsigned NOT NULL DEFAULT 0,
  `blm` smallint(5) unsigned NOT NULL DEFAULT 0,
  `rdm` smallint(5) unsigned NOT NULL DEFAULT 0,
  `thf` smallint(5) unsigned NOT NULL DEFAULT 0,
  `pld` smallint(5) unsigned NOT NULL DEFAULT 0,
  `drk` smallint(5) unsigned NOT NULL DEFAULT 0,
  `bst` smallint(5) unsigned NOT NULL DEFAULT 0,
  `brd` smallint(5) unsigned NOT NULL DEFAULT 0,
  `rng` smallint(5) unsigned NOT NULL DEFAULT 0,
  `sam` smallint(5) unsigned NOT NULL DEFAULT 0,
  `nin` smallint(5) unsigned NOT NULL DEFAULT 0,
  `drg` smallint(5) unsigned NOT NULL DEFAULT 0,
  `smn` smallint(5) unsigned NOT NULL DEFAULT 0,
  `blu` smallint(5) unsigned NOT NULL DEFAULT 0,
  `cor` smallint(5) unsigned NOT NULL DEFAULT 0,
  `pup` smallint(5) unsigned NOT NULL DEFAULT 0,
  `dnc` smallint(5) unsigned NOT NULL DEFAULT 0,
  `sch` smallint(5) unsigned NOT NULL DEFAULT 0,
  `geo` smallint(5) unsigned NOT NULL DEFAULT 0,
  `run` smallint(5) unsigned NOT NULL DEFAULT 0,
  `merits` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `limits` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=85;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_inventory`
--

DROP TABLE IF EXISTS `char_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_inventory` (
  `charid` int(10) unsigned NOT NULL,
  `location` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `slot` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `itemId` smallint(5) unsigned NOT NULL DEFAULT 65535,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `bazaar` int(8) unsigned NOT NULL DEFAULT 0,
  `signature` varchar(20) NOT NULL DEFAULT '',
  `extra` tinyblob DEFAULT NULL,
  PRIMARY KEY (`charid`,`location`,`slot`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=28;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_jobs`
--

DROP TABLE IF EXISTS `char_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_jobs` (
  `charid` int(10) unsigned NOT NULL,
  `unlocked` int(10) unsigned NOT NULL DEFAULT 126,
  `genkai` tinyint(2) unsigned NOT NULL DEFAULT 50,
  `war` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `mnk` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `whm` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `blm` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `rdm` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `thf` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `pld` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `drk` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `bst` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `brd` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `rng` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `sam` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `nin` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `drg` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `smn` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `blu` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `cor` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `pup` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `dnc` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `sch` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `geo` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `run` tinyint(2) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=95;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_look`
--

DROP TABLE IF EXISTS `char_look`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_look` (
  `charid` int(10) unsigned NOT NULL,
  `face` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `race` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `size` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `head` smallint(4) unsigned NOT NULL DEFAULT 0,
  `body` smallint(4) unsigned NOT NULL DEFAULT 8,
  `hands` smallint(4) unsigned NOT NULL DEFAULT 8,
  `legs` smallint(4) unsigned NOT NULL DEFAULT 8,
  `feet` smallint(4) unsigned NOT NULL DEFAULT 8,
  `main` smallint(4) unsigned NOT NULL DEFAULT 0,
  `sub` smallint(4) unsigned NOT NULL DEFAULT 0,
  `ranged` smallint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=24;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_merit`
--

DROP TABLE IF EXISTS `char_merit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_merit` (
  `charid` int(10) unsigned NOT NULL,
  `meritid` smallint(5) unsigned NOT NULL,
  `upgrades` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `idx_char_merit_meritid_charid` (`meritid`,`charid`),
  KEY `char_merits_charid_index` (`charid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_pet`
--

DROP TABLE IF EXISTS `char_pet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_pet` (
  `charid` int(10) unsigned NOT NULL,
  `wyvernid` smallint(3) unsigned NOT NULL DEFAULT 0,
  `automatonid` smallint(3) unsigned NOT NULL DEFAULT 0,
  `unlocked_attachments` blob DEFAULT NULL,
  `equipped_attachments` blob DEFAULT NULL,
  `adventuringfellowid` smallint(3) unsigned NOT NULL DEFAULT 0,
  `chocoboid` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_points`
--

DROP TABLE IF EXISTS `char_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_points` (
  `charid` int(10) unsigned NOT NULL,
  `sandoria_cp` int(10) unsigned NOT NULL DEFAULT 0,
  `bastok_cp` int(10) unsigned NOT NULL DEFAULT 0,
  `windurst_cp` int(10) unsigned NOT NULL DEFAULT 0,
  `beastman_seal` int(10) unsigned NOT NULL DEFAULT 0,
  `kindred_seal` smallint(5) unsigned NOT NULL DEFAULT 0,
  `kindred_crest` smallint(5) unsigned NOT NULL DEFAULT 0,
  `high_kindred_crest` smallint(5) unsigned NOT NULL DEFAULT 0,
  `sacred_kindred_crest` smallint(5) unsigned NOT NULL DEFAULT 0,
  `ancient_beastcoin` smallint(5) unsigned NOT NULL DEFAULT 0,
  `valor_point` smallint(5) unsigned NOT NULL DEFAULT 0,
  `scyld` smallint(5) unsigned NOT NULL DEFAULT 0,
  `guild_fishing` int(10) unsigned NOT NULL DEFAULT 0,
  `guild_woodworking` int(10) unsigned NOT NULL DEFAULT 0,
  `guild_smithing` int(10) unsigned NOT NULL DEFAULT 0,
  `guild_goldsmithing` int(10) unsigned NOT NULL DEFAULT 0,
  `guild_weaving` int(10) unsigned NOT NULL DEFAULT 0,
  `guild_leathercraft` int(10) unsigned NOT NULL DEFAULT 0,
  `guild_bonecraft` int(10) unsigned NOT NULL DEFAULT 0,
  `guild_alchemy` int(10) unsigned NOT NULL DEFAULT 0,
  `guild_cooking` int(10) unsigned NOT NULL DEFAULT 0,
  `cinder` int(10) unsigned NOT NULL DEFAULT 0,
  `fire_fewell` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `ice_fewell` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `wind_fewell` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `earth_fewell` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `lightning_fewell` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `water_fewell` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `light_fewell` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `dark_fewell` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `ballista_point` int(10) unsigned NOT NULL DEFAULT 0,
  `fellow_point` int(10) unsigned NOT NULL DEFAULT 0,
  `chocobuck_sandoria` smallint(4) unsigned NOT NULL DEFAULT 0,
  `chocobuck_bastok` smallint(4) unsigned NOT NULL DEFAULT 0,
  `chocobuck_windurst` smallint(4) unsigned NOT NULL DEFAULT 0,
  `research_mark` int(10) unsigned NOT NULL DEFAULT 0,
  `tunnel_worm` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `morion_worm` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `phantom_worm` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `moblin_marble` int(10) unsigned NOT NULL DEFAULT 0,
  `infamy` smallint(5) unsigned NOT NULL DEFAULT 0,
  `prestige` smallint(5) unsigned NOT NULL DEFAULT 0,
  `legion_point` int(10) unsigned NOT NULL DEFAULT 0,
  `spark_of_eminence` int(10) unsigned NOT NULL DEFAULT 0,
  `shining_star` int(10) unsigned NOT NULL DEFAULT 0,
  `imperial_standing` int(10) unsigned NOT NULL DEFAULT 0,
  `leujaoam_assault_point` int(10) unsigned NOT NULL DEFAULT 0,
  `mamool_assault_point` int(10) unsigned NOT NULL DEFAULT 0,
  `lebros_assault_point` int(10) unsigned NOT NULL DEFAULT 0,
  `periqia_assault_point` int(10) unsigned NOT NULL DEFAULT 0,
  `ilrusi_assault_point` int(10) unsigned NOT NULL DEFAULT 0,
  `nyzul_isle_assault_point` int(10) unsigned NOT NULL DEFAULT 0,
  `zeni_point` int(10) unsigned NOT NULL DEFAULT 0,
  `jetton` int(10) unsigned NOT NULL DEFAULT 0,
  `therion_ichor` int(10) unsigned NOT NULL DEFAULT 0,
  `allied_notes` int(10) unsigned NOT NULL DEFAULT 0,
  `bayld` int(10) unsigned NOT NULL DEFAULT 0,
  `kinetic_unit` smallint(5) unsigned NOT NULL DEFAULT 0,
  `obsidian_fragment` int(10) unsigned NOT NULL DEFAULT 0,
  `lebondopt_wing` smallint(5) unsigned NOT NULL DEFAULT 0,
  `pulchridopt_wing` smallint(5) unsigned NOT NULL DEFAULT 0,
  `mweya_plasm` int(10) unsigned NOT NULL DEFAULT 0,
  `cruor` int(10) unsigned NOT NULL DEFAULT 0,
  `resistance_credit` int(10) unsigned NOT NULL DEFAULT 0,
  `dominion_note` int(10) unsigned NOT NULL DEFAULT 0,
  `fifth_echelon_trophy` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `fourth_echelon_trophy` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `third_echelon_trophy` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `second_echelon_trophy` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `first_echelon_trophy` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `cave_points` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `id_tags` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `op_credits` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `traverser_stones` int(10) unsigned NOT NULL DEFAULT 0,
  `voidstones` int(10) unsigned NOT NULL DEFAULT 0,
  `kupofried_corundums` int(10) unsigned NOT NULL DEFAULT 0,
  `imprimaturs` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `pheromone_sacks` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `fire_crystals` smallint(5) unsigned NOT NULL DEFAULT 0,
  `ice_crystals` smallint(5) unsigned NOT NULL DEFAULT 0,
  `wind_crystals` smallint(5) unsigned NOT NULL DEFAULT 0,
  `earth_crystals` smallint(5) unsigned NOT NULL DEFAULT 0,
  `lightning_crystals` smallint(5) unsigned NOT NULL DEFAULT 0,
  `water_crystals` smallint(5) unsigned NOT NULL DEFAULT 0,
  `light_crystals` smallint(5) unsigned NOT NULL DEFAULT 0,
  `dark_crystals` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_profile`
--

DROP TABLE IF EXISTS `char_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_profile` (
  `charid` int(10) unsigned NOT NULL,
  `rank_points` int(10) unsigned NOT NULL DEFAULT 0,
  `rank_sandoria` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `rank_bastok` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `rank_windurst` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `fame_sandoria` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_bastok` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_windurst` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_norg` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_jeuno` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_aby_konschtat` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_aby_tahrongi` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_aby_latheine` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_aby_misareaux` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_aby_vunkerl` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_aby_attohwa` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_aby_altepa` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_aby_grauberg` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_aby_uleguerand` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_adoulin` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_recast`
--

DROP TABLE IF EXISTS `char_recast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_recast` (
  `charid` int(10) unsigned NOT NULL,
  `id` smallint(5) NOT NULL DEFAULT 0,
  `time` int(10) NOT NULL DEFAULT 0,
  `recast` smallint(5) NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_skills`
--

DROP TABLE IF EXISTS `char_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_skills` (
  `charid` int(10) unsigned NOT NULL,
  `skillid` tinyint(2) unsigned NOT NULL,
  `value` smallint(4) unsigned NOT NULL DEFAULT 0,
  `rank` tinyint(2) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`,`skillid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=10;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_spells`
--

DROP TABLE IF EXISTS `char_spells`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_spells` (
  `charid` int(10) unsigned NOT NULL,
  `spellid` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `idx_char_spells_spellid_charid` (`spellid`,`charid`),
  KEY `char_spells_charid_index` (`charid`),
  KEY `char_spells_spellid_index` (`spellid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_stats`
--

DROP TABLE IF EXISTS `char_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_stats` (
  `charid` int(10) unsigned NOT NULL,
  `hp` smallint(4) unsigned NOT NULL DEFAULT 50,
  `mp` smallint(4) unsigned NOT NULL DEFAULT 50,
  `nameflags` int(10) unsigned NOT NULL DEFAULT 0,
  `mhflag` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `mjob` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `sjob` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `death` int(10) unsigned NOT NULL DEFAULT 0,
  `2h` int(10) unsigned NOT NULL DEFAULT 0,
  `title` smallint(4) unsigned NOT NULL DEFAULT 0,
  `bazaar_message` blob DEFAULT NULL,
  `zoning` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `mlvl` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `slvl` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `pet_id` smallint(3) unsigned NOT NULL DEFAULT 0,
  `pet_type` smallint(3) unsigned NOT NULL DEFAULT 0,
  `pet_hp` smallint(4) unsigned NOT NULL DEFAULT 0,
  `pet_mp` smallint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_storage`
--

DROP TABLE IF EXISTS `char_storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_storage` (
  `charid` int(10) unsigned NOT NULL,
  `inventory` tinyint(2) unsigned NOT NULL DEFAULT 30,
  `safe` tinyint(2) unsigned NOT NULL DEFAULT 50,
  `locker` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `satchel` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `sack` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `case` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `wardrobe` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `wardrobe2` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `wardrobe3` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `wardrobe4` tinyint(2) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_style`
--

DROP TABLE IF EXISTS `char_style`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_style` (
  `charid` int(10) unsigned NOT NULL,
  `head` smallint(5) unsigned NOT NULL DEFAULT 0,
  `body` smallint(5) unsigned NOT NULL DEFAULT 0,
  `hands` smallint(5) unsigned NOT NULL DEFAULT 0,
  `legs` smallint(5) unsigned NOT NULL DEFAULT 0,
  `feet` smallint(5) unsigned NOT NULL DEFAULT 0,
  `main` smallint(5) unsigned NOT NULL DEFAULT 0,
  `sub` smallint(5) unsigned NOT NULL DEFAULT 0,
  `ranged` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=20;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_unlocks`
--

DROP TABLE IF EXISTS `char_unlocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_unlocks` (
  `charid` int(10) unsigned NOT NULL,
  `outpost_sandy` int(10) unsigned NOT NULL DEFAULT 0,
  `outpost_bastok` int(10) unsigned NOT NULL DEFAULT 0,
  `outpost_windy` int(10) unsigned NOT NULL DEFAULT 0,
  `mog_locker` int(10) unsigned NOT NULL DEFAULT 0,
  `runic_portal` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `maw` int(10) unsigned NOT NULL DEFAULT 0,
  `campaign_sandy` int(10) unsigned NOT NULL DEFAULT 0,
  `campaign_bastok` int(10) unsigned NOT NULL DEFAULT 0,
  `campaign_windy` int(10) unsigned NOT NULL DEFAULT 0,
  `homepoints` blob DEFAULT NULL,
  `survivals` blob DEFAULT NULL,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `char_vars`
--

DROP TABLE IF EXISTS `char_vars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `char_vars` (
  `charid` int(10) unsigned NOT NULL,
  `varname` varchar(30) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`charid`,`varname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chars`
--

DROP TABLE IF EXISTS `chars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chars` (
  `charid` int(10) unsigned NOT NULL,
  `accid` int(10) unsigned NOT NULL,
  `charname` varchar(15) NOT NULL,
  `nation` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `pos_zone` smallint(3) unsigned NOT NULL,
  `pos_prevzone` smallint(3) unsigned NOT NULL DEFAULT 0,
  `pos_rot` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `pos_x` float(7,3) NOT NULL DEFAULT 0.000,
  `pos_y` float(7,3) NOT NULL DEFAULT 0.000,
  `pos_z` float(7,3) NOT NULL DEFAULT 0.000,
  `moghouse` int(10) unsigned NOT NULL DEFAULT 0,
  `boundary` smallint(5) unsigned NOT NULL DEFAULT 0,
  `home_zone` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `home_rot` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `home_x` float(7,3) NOT NULL DEFAULT 0.000,
  `home_y` float(7,3) NOT NULL DEFAULT 0.000,
  `home_z` float(7,3) NOT NULL DEFAULT 0.000,
  `missions` blob DEFAULT NULL,
  `assault` blob DEFAULT NULL,
  `campaign` blob DEFAULT NULL,
  `quests` blob DEFAULT NULL,
  `keyitems` blob DEFAULT NULL,
  `set_blue_spells` blob DEFAULT NULL,
  `abilities` blob DEFAULT NULL,
  `weaponskills` blob DEFAULT NULL,
  `titles` blob DEFAULT NULL,
  `zones` blob DEFAULT NULL,
  `playtime` int(10) unsigned NOT NULL DEFAULT 0,
  `unlocked_weapons` blob DEFAULT NULL,
  `gmlevel` smallint(3) unsigned NOT NULL DEFAULT 0,
  `mentor` smallint(3) NOT NULL DEFAULT 0,
  `campaign_allegiance` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `isstylelocked` tinyint(1) NOT NULL DEFAULT 0,
  `nnameflags` int(10) unsigned NOT NULL DEFAULT 0,
  `moghancement` smallint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`),
  FULLTEXT KEY `charname` (`charname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER char_insert
    BEFORE INSERT ON chars
    FOR EACH ROW
BEGIN
    INSERT INTO `char_equip`     SET `charid` = NEW.charid;
    INSERT INTO `char_exp`       SET `charid` = NEW.charid;
    INSERT INTO `char_jobs`      SET `charid` = NEW.charid;
    INSERT INTO `char_pet`       SET `charid` = NEW.charid;
    INSERT INTO `char_points`    SET `charid` = NEW.charid;
    INSERT INTO `char_unlocks`   SET `charid` = NEW.charid;
    INSERT INTO `char_profile`   SET `charid` = NEW.charid;
    INSERT INTO `char_storage`   SET `charid` = NEW.charid;
    INSERT INTO `char_inventory` SET `charid` = NEW.charid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER char_delete
    BEFORE DELETE ON chars
    FOR EACH ROW
BEGIN
    DELETE FROM `char_blacklist` WHERE `charid_owner` = OLD.charid;
    DELETE FROM `char_effects`   WHERE `charid` = OLD.charid;
    DELETE FROM `char_equip`     WHERE `charid` = OLD.charid;
    DELETE FROM `char_exp`       WHERE `charid` = OLD.charid;
    DELETE FROM `char_inventory` WHERE `charid` = OLD.charid;
    DELETE FROM `char_jobs`      WHERE `charid` = OLD.charid;
    DELETE FROM `char_look`      WHERE `charid` = OLD.charid;
    DELETE FROM `char_merit`     WHERE `charid` = OLD.charid;
    DELETE FROM `char_pet`       WHERE `charid` = OLD.charid;
    DELETE FROM `char_points`    WHERE `charid` = OLD.charid;
    DELETE FROM `char_unlocks`   WHERE `charid` = OLD.charid;
    DELETE FROM `char_profile`   WHERE `charid` = OLD.charid;
    DELETE FROM `char_recast`    WHERE `charid` = OLD.charid;
    DELETE FROM `char_skills`    WHERE `charid` = OLD.charid;
    DELETE FROM `char_spells`    WHERE `charid` = OLD.charid;
    DELETE FROM `char_stats`     WHERE `charid` = OLD.charid;
    DELETE FROM `char_storage`   WHERE `charid` = OLD.charid;
    DELETE FROM `char_style`     WHERE `charid` = OLD.charid;
    DELETE FROM `char_vars`      WHERE `charid` = OLD.charid;
    DELETE FROM `auction_house`  WHERE `seller` = OLD.charid;
    DELETE FROM `delivery_box`   WHERE `charid` = OLD.charid;
    UPDATE `account_ip_record` SET `charid`  = 0 where `charid` = OLD.charid;
    UPDATE `delivery_box` SET sent = 0 WHERE box = 2 AND received = 0 AND sent = 1 AND senderid = OLD.charid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cheat_incidents`
--

DROP TABLE IF EXISTS `cheat_incidents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cheat_incidents` (
  `charid` int(10) unsigned NOT NULL,
  `incident_time` datetime NOT NULL DEFAULT current_timestamp(),
  `cheatid` int(10) unsigned NOT NULL,
  `cheatarg` int(10) unsigned NOT NULL,
  `description` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cheat_types`
--

DROP TABLE IF EXISTS `cheat_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cheat_types` (
  `cheatid` int(10) unsigned NOT NULL,
  `name` varchar(15) NOT NULL,
  `description` varchar(128) NOT NULL,
  `argument` int(10) unsigned NOT NULL,
  `action_bitmask` int(10) unsigned NOT NULL,
  `warning_message` varchar(128) NOT NULL,
  PRIMARY KEY (`cheatid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conquest_system`
--

DROP TABLE IF EXISTS `conquest_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conquest_system` (
  `region_id` tinyint(2) NOT NULL DEFAULT 0,
  `region_control` tinyint(2) NOT NULL DEFAULT 0,
  `region_control_prev` tinyint(2) NOT NULL DEFAULT 0,
  `sandoria_influence` int(10) NOT NULL DEFAULT 0,
  `bastok_influence` int(10) NOT NULL DEFAULT 0,
  `windurst_influence` int(10) NOT NULL DEFAULT 0,
  `beastmen_influence` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `delivery_box`
--

DROP TABLE IF EXISTS `delivery_box`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `delivery_box` (
  `charid` int(10) unsigned NOT NULL,
  `charname` varchar(15) DEFAULT NULL,
  `box` tinyint(1) unsigned NOT NULL,
  `slot` smallint(3) unsigned NOT NULL DEFAULT 0,
  `itemid` smallint(5) unsigned NOT NULL,
  `itemsubid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `quantity` int(10) unsigned NOT NULL,
  `extra` tinyblob DEFAULT NULL,
  `senderid` int(10) unsigned NOT NULL DEFAULT 0,
  `sender` varchar(15) DEFAULT NULL,
  `received` tinyint(1) NOT NULL DEFAULT 0,
  `sent` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`,`box`,`slot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER delivery_box_insert
    BEFORE INSERT ON delivery_box
    FOR EACH ROW
BEGIN
    SET @slot := 0;
    SELECT MAX(slot) INTO @slot FROM delivery_box WHERE box = NEW.box AND charid = NEW.charid;
    IF NEW.box = 1 THEN
    IF @slot IS NULL OR @slot < 8 THEN SET NEW.slot := 8; ELSE SET NEW.slot := @slot + 1; END IF;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `despoil_effects`
--

DROP TABLE IF EXISTS `despoil_effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `despoil_effects` (
  `itemId` smallint(5) unsigned NOT NULL,
  `effectId` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exp_base`
--

DROP TABLE IF EXISTS `exp_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exp_base` (
  `level` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `exp` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=9;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exp_table`
--

DROP TABLE IF EXISTS `exp_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exp_table` (
  `level` tinyint(2) NOT NULL,
  `r1` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r2` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r3` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r4` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r5` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r6` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r7` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r8` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r9` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r10` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r11` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r12` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r13` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r14` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r15` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r16` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r17` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r18` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r19` smallint(4) unsigned NOT NULL DEFAULT 0,
  `r20` smallint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=65;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_area`
--

DROP TABLE IF EXISTS `fishing_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_area` (
  `zoneid` smallint(5) unsigned NOT NULL,
  `areaid` smallint(5) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `bound_type` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `bound_height` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `bound_radius` smallint(5) unsigned NOT NULL DEFAULT 0,
  `bounds` blob DEFAULT NULL,
  `center_x` float(7,3) NOT NULL DEFAULT 0.000,
  `center_y` float(7,3) NOT NULL DEFAULT 0.000,
  `center_z` float(7,3) NOT NULL DEFAULT 0.000,
  PRIMARY KEY (`zoneid`,`areaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_catch`
--

DROP TABLE IF EXISTS `fishing_catch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_catch` (
  `zoneid` smallint(5) unsigned NOT NULL,
  `areaid` tinyint(3) unsigned NOT NULL,
  `groupid` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`zoneid`,`areaid`,`groupid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=27;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_fish`
--

DROP TABLE IF EXISTS `fishing_fish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_fish` (
  `fishid` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `min_skill_level` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `skill_level` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `size` tinyint(3) unsigned NOT NULL,
  `base_delay` tinyint(2) unsigned NOT NULL,
  `base_move` tinyint(2) unsigned NOT NULL,
  `min_length` smallint(5) unsigned NOT NULL DEFAULT 1,
  `max_length` smallint(5) unsigned NOT NULL DEFAULT 1,
  `size_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `water_type` tinyint(2) unsigned NOT NULL,
  `log` tinyint(3) unsigned NOT NULL DEFAULT 255,
  `quest` tinyint(3) unsigned NOT NULL DEFAULT 255,
  `flags` int(11) unsigned NOT NULL DEFAULT 0,
  `legendary` int(2) unsigned NOT NULL DEFAULT 0,
  `legendary_flags` int(11) unsigned NOT NULL DEFAULT 0,
  `item` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `max_hook` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `rarity` smallint(5) unsigned NOT NULL DEFAULT 0,
  `required_keyitem` smallint(5) unsigned NOT NULL,
  `required_catches` blob NOT NULL,
  `disabled` tinyint(2) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`fishid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=23;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_group`
--

DROP TABLE IF EXISTS `fishing_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_group` (
  `groupid` int(10) unsigned NOT NULL,
  `fishid` int(10) unsigned NOT NULL,
  `rarity` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `requiredmoon` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `moonbonus` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `requiredtime` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `timebonus` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `requiredday` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `daybonus` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `requiredweather` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `weatherbonus` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `fishing_groupcol` varchar(45) DEFAULT '0',
  PRIMARY KEY (`groupid`,`fishid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_log`
--

DROP TABLE IF EXISTS `fishing_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_log` (
  `logid` int(11) NOT NULL AUTO_INCREMENT,
  `zone` smallint(4) NOT NULL DEFAULT 0,
  `area` tinyint(2) NOT NULL DEFAULT 0,
  `charid` int(11) unsigned NOT NULL DEFAULT 0,
  `charname` varchar(15) NOT NULL,
  `charlevel` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `charskill` smallint(5) unsigned NOT NULL DEFAULT 0,
  `pos_x` float NOT NULL DEFAULT 0,
  `pos_y` float NOT NULL DEFAULT 0,
  `pos_z` float NOT NULL DEFAULT 0,
  `catchtype` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `catchid` int(11) unsigned NOT NULL DEFAULT 0,
  `catchname` tinytext NOT NULL,
  `catchskill` smallint(5) unsigned NOT NULL DEFAULT 0,
  `regen` smallint(4) unsigned NOT NULL DEFAULT 0,
  `catchtime` datetime NOT NULL DEFAULT current_timestamp(),
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=183261 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_lure`
--

DROP TABLE IF EXISTS `fishing_lure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_lure` (
  `lureid` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `luretype` tinyint(2) unsigned NOT NULL,
  `maxhook` tinyint(2) unsigned NOT NULL,
  `losable` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `flags` int(11) unsigned NOT NULL DEFAULT 0,
  `mmm` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`lureid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_lure_affinity`
--

DROP TABLE IF EXISTS `fishing_lure_affinity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_lure_affinity` (
  `lureid` smallint(5) unsigned NOT NULL,
  `fishid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `power` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`lureid`,`fishid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=28;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_mob`
--

DROP TABLE IF EXISTS `fishing_mob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_mob` (
  `mobid` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `zoneid` smallint(5) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL,
  `size` tinyint(3) unsigned NOT NULL,
  `base_delay` tinyint(2) unsigned NOT NULL,
  `base_move` tinyint(2) unsigned NOT NULL,
  `log` tinyint(3) unsigned NOT NULL DEFAULT 255,
  `quest` tinyint(3) unsigned NOT NULL DEFAULT 255,
  `nm` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `nm_flags` int(11) unsigned NOT NULL DEFAULT 0,
  `areaid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `rarity` smallint(5) unsigned NOT NULL DEFAULT 0,
  `min_respawn` int(10) unsigned NOT NULL DEFAULT 0,
  `required_lureid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `required_key_item` smallint(5) unsigned NOT NULL DEFAULT 0,
  `disabled` tinyint(2) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`mobid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_rod`
--

DROP TABLE IF EXISTS `fishing_rod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_rod` (
  `rodid` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `material` tinyint(2) unsigned NOT NULL,
  `size_type` tinyint(3) unsigned NOT NULL,
  `flags` int(11) unsigned NOT NULL DEFAULT 0,
  `durability` tinyint(3) unsigned NOT NULL DEFAULT 10,
  `fish_attack` tinyint(3) unsigned NOT NULL,
  `lgd_bonus_attack` tinyint(3) unsigned NOT NULL,
  `miss_regen` tinyint(3) unsigned NOT NULL,
  `lgd_miss_regen` tinyint(3) unsigned NOT NULL,
  `fish_time` tinyint(3) unsigned NOT NULL,
  `lgd_bonus_time` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `sm_delay_bonus` tinyint(2) unsigned NOT NULL,
  `sm_move_bonus` tinyint(2) unsigned NOT NULL,
  `lg_delay_bonus` tinyint(2) unsigned NOT NULL,
  `lg_move_bonus` tinyint(2) unsigned NOT NULL,
  `multiplier` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `breakable` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `broken_rodid` int(10) unsigned NOT NULL,
  `mmm` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`rodid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishing_zone`
--

DROP TABLE IF EXISTS `fishing_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fishing_zone` (
  `zoneid` smallint(5) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `difficulty` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`zoneid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flist`
--

DROP TABLE IF EXISTS `flist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flist` (
  `callingchar` int(10) unsigned NOT NULL DEFAULT 0,
  `listedchar` int(10) unsigned NOT NULL DEFAULT 0,
  `status` int(4) NOT NULL DEFAULT 0,
  `name` varchar(32) DEFAULT NULL,
  `note` varchar(16) DEFAULT 'actualempty'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flist_settings`
--

DROP TABLE IF EXISTS `flist_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flist_settings` (
  `callingchar` int(10) unsigned NOT NULL DEFAULT 0,
  `visible` int(2) unsigned NOT NULL DEFAULT 1,
  `notifications` int(2) unsigned NOT NULL DEFAULT 1,
  `channel` int(4) unsigned NOT NULL DEFAULT 29,
  `size` int(4) unsigned NOT NULL DEFAULT 3,
  `lastcall` int(10) unsigned NOT NULL DEFAULT 0,
  `lastonline` int(10) unsigned NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guild_item_points`
--

DROP TABLE IF EXISTS `guild_item_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guild_item_points` (
  `guildid` tinyint(1) unsigned NOT NULL,
  `itemid` smallint(5) unsigned NOT NULL,
  `rank` smallint(1) unsigned NOT NULL,
  `points` smallint(5) unsigned NOT NULL DEFAULT 0,
  `max_points` smallint(5) unsigned NOT NULL DEFAULT 0,
  `pattern` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`guildid`,`itemid`,`pattern`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guild_shops`
--

DROP TABLE IF EXISTS `guild_shops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guild_shops` (
  `guildid` smallint(5) unsigned NOT NULL,
  `itemid` smallint(5) unsigned NOT NULL,
  `min_price` int(10) unsigned NOT NULL DEFAULT 0,
  `max_price` int(10) unsigned NOT NULL DEFAULT 0,
  `max_quantity` smallint(5) unsigned NOT NULL DEFAULT 0,
  `daily_increase` smallint(5) unsigned NOT NULL DEFAULT 0,
  `initial_quantity` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`guildid`,`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guilds`
--

DROP TABLE IF EXISTS `guilds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guilds` (
  `id` tinyint(1) unsigned NOT NULL,
  `points_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=13 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instance_entities`
--

DROP TABLE IF EXISTS `instance_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `instance_entities` (
  `instanceid` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`instanceid`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instance_list`
--

DROP TABLE IF EXISTS `instance_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `instance_list` (
  `instanceid` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `instance_name` varchar(35) NOT NULL DEFAULT '',
  `entrance_zone` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `time_limit` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `start_x` float(7,3) NOT NULL DEFAULT 0.000,
  `start_y` float(7,3) NOT NULL DEFAULT 0.000,
  `start_z` float(7,3) NOT NULL DEFAULT 0.000,
  `start_rot` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `music_day` smallint(3) NOT NULL DEFAULT -1,
  `music_night` smallint(3) NOT NULL DEFAULT -1,
  `battlesolo` smallint(3) NOT NULL DEFAULT -1,
  `battlemulti` smallint(3) NOT NULL DEFAULT -1,
  PRIMARY KEY (`instanceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_basic`
--

DROP TABLE IF EXISTS `item_basic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_basic` (
  `itemid` smallint(5) unsigned NOT NULL,
  `subid` smallint(4) unsigned NOT NULL DEFAULT 0,
  `name` tinytext NOT NULL,
  `sortname` tinytext NOT NULL,
  `stackSize` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `flags` smallint(5) unsigned NOT NULL DEFAULT 0,
  `aH` tinyint(2) unsigned NOT NULL DEFAULT 99,
  `NoSale` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `BaseSell` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=34 PACK_KEYS=1 CHECKSUM=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_equipment`
--

DROP TABLE IF EXISTS `item_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_equipment` (
  `itemId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `name` tinytext DEFAULT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `ilevel` tinyint(3) NOT NULL DEFAULT 0,
  `jobs` int(10) unsigned NOT NULL DEFAULT 0,
  `MId` smallint(3) unsigned NOT NULL DEFAULT 0,
  `shieldSize` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `scriptType` smallint(5) unsigned NOT NULL DEFAULT 0,
  `slot` smallint(5) unsigned NOT NULL DEFAULT 0,
  `rslot` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=37 PACK_KEYS=1 CHECKSUM=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_furnishing`
--

DROP TABLE IF EXISTS `item_furnishing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_furnishing` (
  `itemid` smallint(5) unsigned NOT NULL,
  `name` text NOT NULL,
  `storage` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `moghancement` smallint(4) unsigned NOT NULL DEFAULT 0,
  `element` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `aura` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=25 PACK_KEYS=1 CHECKSUM=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_latents`
--

DROP TABLE IF EXISTS `item_latents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_latents` (
  `itemId` smallint(5) unsigned NOT NULL,
  `modId` smallint(5) unsigned NOT NULL,
  `value` smallint(5) NOT NULL DEFAULT 0,
  `latentId` smallint(5) NOT NULL,
  `latentParam` smallint(5) NOT NULL,
  PRIMARY KEY (`itemId`,`modId`,`value`,`latentId`,`latentParam`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=13 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_mods`
--

DROP TABLE IF EXISTS `item_mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_mods` (
  `itemId` smallint(5) unsigned NOT NULL,
  `modId` smallint(5) unsigned NOT NULL,
  `value` smallint(5) NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemId`,`modId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=13 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_mods_pet`
--

DROP TABLE IF EXISTS `item_mods_pet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_mods_pet` (
  `itemId` smallint(5) unsigned NOT NULL,
  `modId` smallint(5) unsigned NOT NULL,
  `value` smallint(5) NOT NULL DEFAULT 0,
  `petType` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemId`,`modId`,`petType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=13 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_puppet`
--

DROP TABLE IF EXISTS `item_puppet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_puppet` (
  `itemid` smallint(5) unsigned NOT NULL,
  `name` tinytext NOT NULL,
  `slot` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `element` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_usable`
--

DROP TABLE IF EXISTS `item_usable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_usable` (
  `itemid` smallint(5) unsigned NOT NULL,
  `name` text NOT NULL,
  `validTargets` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `activation` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `animation` smallint(4) unsigned NOT NULL DEFAULT 0,
  `animationTime` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `maxCharges` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `useDelay` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `reuseDelay` int(10) unsigned NOT NULL DEFAULT 0,
  `aoe` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=1 CHECKSUM=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_weapon`
--

DROP TABLE IF EXISTS `item_weapon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_weapon` (
  `itemId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `name` text DEFAULT NULL,
  `skill` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `subskill` tinyint(2) NOT NULL DEFAULT 0,
  `ilvl_skill` smallint(3) NOT NULL DEFAULT 0,
  `ilvl_parry` smallint(3) NOT NULL DEFAULT 0,
  `ilvl_macc` smallint(3) NOT NULL DEFAULT 0,
  `dmgType` int(10) unsigned NOT NULL DEFAULT 0,
  `hit` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `delay` int(10) NOT NULL DEFAULT 0,
  `dmg` int(10) unsigned NOT NULL DEFAULT 0,
  `unlock_points` smallint(5) NOT NULL DEFAULT 0,
  `category` int(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=54 PACK_KEYS=1 CHECKSUM=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `job_points`
--

DROP TABLE IF EXISTS `job_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_points` (
  `job_pointid` smallint(10) unsigned NOT NULL,
  `name` varchar(40) NOT NULL,
  `upgrade` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `jobs` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`job_pointid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `linkshells`
--

DROP TABLE IF EXISTS `linkshells`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `linkshells` (
  `linkshellid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `color` smallint(5) unsigned NOT NULL DEFAULT 61440,
  `poster` varchar(15) NOT NULL DEFAULT '',
  `message` blob DEFAULT NULL,
  `messagetime` int(10) unsigned NOT NULL DEFAULT 0,
  `postrights` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `broken` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`linkshellid`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `merits`
--

DROP TABLE IF EXISTS `merits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `merits` (
  `meritid` smallint(5) unsigned NOT NULL,
  `name` char(25) NOT NULL,
  `upgrade` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `value` smallint(5) NOT NULL DEFAULT 0,
  `jobs` int(10) unsigned NOT NULL DEFAULT 0,
  `upgradeid` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `catagoryid` tinyint(2) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`meritid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_droplist`
--

DROP TABLE IF EXISTS `mob_droplist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_droplist` (
  `dropId` smallint(5) unsigned NOT NULL,
  `dropType` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `groupId` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `groupRate` smallint(4) unsigned NOT NULL DEFAULT 1000,
  `itemId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `itemRate` smallint(4) unsigned NOT NULL DEFAULT 0,
  KEY `dropId` (`dropId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=9;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_family_mods`
--

DROP TABLE IF EXISTS `mob_family_mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_family_mods` (
  `familyid` smallint(5) unsigned NOT NULL,
  `modid` smallint(5) unsigned NOT NULL,
  `value` smallint(5) NOT NULL DEFAULT 0,
  `is_mob_mod` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`familyid`,`modid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=13 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_family_system`
--

DROP TABLE IF EXISTS `mob_family_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_family_system` (
  `familyid` smallint(4) unsigned NOT NULL,
  `family` tinytext DEFAULT NULL,
  `systemid` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `system` tinytext DEFAULT NULL,
  `mobsize` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `speed` tinyint(3) unsigned NOT NULL DEFAULT 40,
  `HP` tinyint(3) unsigned NOT NULL DEFAULT 100,
  `MP` tinyint(3) unsigned NOT NULL DEFAULT 100,
  `STR` smallint(4) unsigned NOT NULL DEFAULT 3,
  `DEX` smallint(4) unsigned NOT NULL DEFAULT 3,
  `VIT` smallint(4) unsigned NOT NULL DEFAULT 3,
  `AGI` smallint(4) unsigned NOT NULL DEFAULT 3,
  `INT` smallint(4) unsigned NOT NULL DEFAULT 3,
  `MND` smallint(4) unsigned NOT NULL DEFAULT 3,
  `CHR` smallint(4) unsigned NOT NULL DEFAULT 3,
  `ATT` smallint(4) unsigned NOT NULL DEFAULT 3,
  `DEF` smallint(4) unsigned NOT NULL DEFAULT 3,
  `ACC` smallint(4) unsigned NOT NULL DEFAULT 3,
  `EVA` smallint(4) unsigned NOT NULL DEFAULT 3,
  `Slash` float NOT NULL DEFAULT 1,
  `Pierce` float NOT NULL DEFAULT 1,
  `H2H` float NOT NULL DEFAULT 1,
  `Impact` float NOT NULL DEFAULT 1,
  `Fire` float NOT NULL DEFAULT 1,
  `Ice` float NOT NULL DEFAULT 1,
  `Wind` float NOT NULL DEFAULT 1,
  `Earth` float NOT NULL DEFAULT 1,
  `Lightning` float NOT NULL DEFAULT 1,
  `Water` float NOT NULL DEFAULT 1,
  `Light` float NOT NULL DEFAULT 1,
  `Dark` float NOT NULL DEFAULT 1,
  `Element` float NOT NULL DEFAULT 0,
  `detects` smallint(5) NOT NULL DEFAULT 0,
  `charmable` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`familyid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=128;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_groups`
--

DROP TABLE IF EXISTS `mob_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_groups` (
  `groupid` int(10) unsigned NOT NULL,
  `poolid` int(10) unsigned NOT NULL DEFAULT 0,
  `zoneid` smallint(3) unsigned NOT NULL DEFAULT 0,
  `name` varchar(24) DEFAULT NULL,
  `respawntime` int(10) unsigned NOT NULL DEFAULT 0,
  `spawntype` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `dropid` int(10) unsigned NOT NULL DEFAULT 0,
  `HP` mediumint(8) NOT NULL DEFAULT 0,
  `MP` mediumint(8) NOT NULL DEFAULT 0,
  `minLevel` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `maxLevel` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `allegiance` tinyint(2) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`zoneid`,`groupid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=22;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_level_exceptions`
--

DROP TABLE IF EXISTS `mob_level_exceptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_level_exceptions` (
  `mobid` int(10) unsigned NOT NULL,
  `minLevel` int(4) unsigned NOT NULL DEFAULT 1,
  `maxLevel` int(4) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`mobid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_pets`
--

DROP TABLE IF EXISTS `mob_pets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_pets` (
  `mob_mobid` int(10) unsigned NOT NULL,
  `pet_offset` int(10) unsigned NOT NULL DEFAULT 1,
  `job` tinyint(4) DEFAULT 9,
  PRIMARY KEY (`mob_mobid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_pool_mods`
--

DROP TABLE IF EXISTS `mob_pool_mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_pool_mods` (
  `poolid` smallint(5) unsigned NOT NULL,
  `modid` smallint(5) unsigned NOT NULL,
  `value` smallint(5) NOT NULL DEFAULT 0,
  `is_mob_mod` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`poolid`,`modid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=13 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_pools`
--

DROP TABLE IF EXISTS `mob_pools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_pools` (
  `poolid` int(10) unsigned NOT NULL,
  `name` varchar(24) DEFAULT NULL,
  `packet_name` varchar(24) DEFAULT NULL,
  `familyid` smallint(4) unsigned NOT NULL DEFAULT 0,
  `modelid` binary(20) NOT NULL,
  `mJob` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `sJob` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `cmbSkill` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `cmbDelay` smallint(3) unsigned NOT NULL DEFAULT 240,
  `cmbDmgMult` smallint(4) unsigned NOT NULL DEFAULT 100,
  `behavior` smallint(5) unsigned NOT NULL DEFAULT 0,
  `aggro` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `true_detection` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `links` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `mobType` smallint(5) unsigned NOT NULL DEFAULT 0,
  `immunity` int(10) NOT NULL DEFAULT 0,
  `name_prefix` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `flag` int(11) unsigned NOT NULL DEFAULT 0,
  `entityFlags` int(11) unsigned NOT NULL DEFAULT 0,
  `animationsub` tinyint(1) NOT NULL DEFAULT 0,
  `hasSpellScript` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `spellList` smallint(4) NOT NULL DEFAULT 0,
  `namevis` tinyint(4) NOT NULL DEFAULT 1,
  `roamflag` smallint(3) unsigned NOT NULL DEFAULT 0,
  `skill_list_id` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`poolid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_skill_lists`
--

DROP TABLE IF EXISTS `mob_skill_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_skill_lists` (
  `skill_list_name` varchar(40) DEFAULT NULL,
  `skill_list_id` smallint(5) unsigned NOT NULL,
  `mob_skill_id` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`skill_list_id`,`mob_skill_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_skills`
--

DROP TABLE IF EXISTS `mob_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_skills` (
  `mob_skill_id` smallint(4) unsigned NOT NULL,
  `mob_anim_id` smallint(4) unsigned NOT NULL,
  `mob_skill_name` varchar(40) CHARACTER SET latin1 NOT NULL,
  `mob_skill_aoe` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `mob_skill_distance` float(3,1) NOT NULL DEFAULT 6.0,
  `mob_anim_time` smallint(4) unsigned NOT NULL DEFAULT 2000,
  `mob_prepare_time` smallint(4) unsigned NOT NULL DEFAULT 1000,
  `mob_valid_targets` smallint(4) unsigned NOT NULL DEFAULT 4,
  `mob_skill_flag` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `mob_skill_param` smallint(5) NOT NULL DEFAULT 0,
  `knockback` tinyint(1) NOT NULL DEFAULT 0,
  `primary_sc` tinyint(4) NOT NULL DEFAULT 0,
  `secondary_sc` tinyint(4) NOT NULL DEFAULT 0,
  `tertiary_sc` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`mob_skill_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_spawn_mods`
--

DROP TABLE IF EXISTS `mob_spawn_mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_spawn_mods` (
  `mobid` int(10) unsigned NOT NULL,
  `modid` smallint(5) unsigned NOT NULL,
  `value` smallint(5) NOT NULL DEFAULT 0,
  `is_mob_mod` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`mobid`,`modid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=13 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_spawn_points`
--

DROP TABLE IF EXISTS `mob_spawn_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_spawn_points` (
  `mobid` int(10) NOT NULL,
  `mobname` varchar(24) DEFAULT NULL,
  `polutils_name` varchar(50) DEFAULT NULL,
  `groupid` int(10) unsigned NOT NULL DEFAULT 0,
  `pos_x` float(7,3) NOT NULL DEFAULT 0.000,
  `pos_y` float(7,3) NOT NULL DEFAULT 0.000,
  `pos_z` float(7,3) NOT NULL DEFAULT 0.000,
  `pos_rot` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`mobid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=33;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_spell_lists`
--

DROP TABLE IF EXISTS `mob_spell_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mob_spell_lists` (
  `spell_list_name` varchar(30) DEFAULT NULL,
  `spell_list_id` smallint(5) unsigned NOT NULL,
  `spell_id` smallint(3) unsigned NOT NULL,
  `min_level` tinyint(3) unsigned NOT NULL,
  `max_level` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`spell_list_id`,`spell_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nm_spawn_points`
--

DROP TABLE IF EXISTS `nm_spawn_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nm_spawn_points` (
  `mobid` int(10) NOT NULL,
  `pos` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `pos_x` float(7,3) NOT NULL DEFAULT 0.000,
  `pos_y` float(7,3) NOT NULL DEFAULT 0.000,
  `pos_z` float(7,3) NOT NULL DEFAULT 0.000,
  PRIMARY KEY (`mobid`,`pos`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `npc_list`
--

DROP TABLE IF EXISTS `npc_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `npc_list` (
  `npcid` int(10) unsigned NOT NULL,
  `name` char(24) DEFAULT NULL,
  `polutils_name` char(50) DEFAULT NULL,
  `pos_rot` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `pos_x` float(7,3) NOT NULL DEFAULT 0.000,
  `pos_y` float(7,3) NOT NULL DEFAULT 0.000,
  `pos_z` float(7,3) NOT NULL DEFAULT 0.000,
  `flag` int(10) unsigned NOT NULL DEFAULT 0,
  `speed` tinyint(3) unsigned NOT NULL DEFAULT 40,
  `speedsub` tinyint(3) unsigned NOT NULL DEFAULT 40,
  `animation` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `animationsub` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `namevis` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `status` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `entityFlags` int(10) unsigned NOT NULL DEFAULT 0,
  `look` binary(20) NOT NULL,
  `name_prefix` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `content_tag` varchar(14) DEFAULT NULL,
  `widescan` tinyint(3) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`npcid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=70 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pet_list`
--

DROP TABLE IF EXISTS `pet_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pet_list` (
  `petid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(15) NOT NULL,
  `poolid` int(10) unsigned NOT NULL DEFAULT 0,
  `minLevel` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `maxLevel` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `time` int(10) unsigned NOT NULL DEFAULT 0,
  `element` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`petid`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pet_name`
--

DROP TABLE IF EXISTS `pet_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pet_name` (
  `id` smallint(3) unsigned NOT NULL,
  `name` char(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `server_variables`
--

DROP TABLE IF EXISTS `server_variables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `server_variables` (
  `name` varchar(50) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skill_caps`
--

DROP TABLE IF EXISTS `skill_caps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skill_caps` (
  `level` tinyint(2) unsigned NOT NULL,
  `r0` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r1` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r2` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r3` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r4` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r5` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r6` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r7` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r8` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r9` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r10` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r11` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r12` smallint(3) unsigned NOT NULL DEFAULT 0,
  `r13` smallint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=32 PACK_KEYS=1 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skill_ranks`
--

DROP TABLE IF EXISTS `skill_ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skill_ranks` (
  `skillid` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `name` char(12) DEFAULT NULL,
  `war` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `mnk` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `whm` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `blm` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `rdm` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `thf` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `pld` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `drk` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `bst` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `brd` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `rng` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `sam` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `nin` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `drg` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `smn` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `blu` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `cor` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `pup` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `dnc` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `sch` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `geo` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `run` tinyint(2) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`skillid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=44 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skillchain_damage_modifiers`
--

DROP TABLE IF EXISTS `skillchain_damage_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skillchain_damage_modifiers` (
  `chain_level` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `chain_count` enum('1','2','3','4','5') NOT NULL DEFAULT '1',
  `initial_modifier` int(3) NOT NULL DEFAULT 1,
  `magic_burst_modifier` int(3) NOT NULL DEFAULT 1,
  PRIMARY KEY (`chain_level`,`chain_count`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spell_list`
--

DROP TABLE IF EXISTS `spell_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spell_list` (
  `spellid` smallint(3) unsigned NOT NULL,
  `name` char(20) NOT NULL,
  `jobs` binary(22) NOT NULL,
  `group` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `family` smallint(4) unsigned NOT NULL DEFAULT 0,
  `element` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `zonemisc` smallint(5) unsigned NOT NULL DEFAULT 0,
  `validTargets` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `skill` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `mpCost` smallint(4) unsigned NOT NULL DEFAULT 0,
  `castTime` smallint(5) unsigned NOT NULL DEFAULT 0,
  `recastTime` int(10) unsigned NOT NULL DEFAULT 0,
  `message` smallint(5) unsigned NOT NULL DEFAULT 0,
  `magicBurstMessage` smallint(5) NOT NULL DEFAULT 0,
  `animation` smallint(5) unsigned NOT NULL DEFAULT 0,
  `animationTime` smallint(4) unsigned NOT NULL DEFAULT 2000,
  `AOE` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `base` smallint(4) unsigned NOT NULL DEFAULT 0,
  `multiplier` float(4,2) NOT NULL DEFAULT 1.00,
  `CE` int(10) unsigned NOT NULL DEFAULT 0,
  `VE` int(10) unsigned NOT NULL DEFAULT 0,
  `requirements` tinyint(2) NOT NULL DEFAULT 0,
  `spell_range` smallint(3) unsigned NOT NULL DEFAULT 0,
  `content_tag` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`spellid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=68;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status_effects`
--

DROP TABLE IF EXISTS `status_effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_effects` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `flags` int(8) unsigned NOT NULL DEFAULT 0,
  `type` smallint(5) unsigned NOT NULL DEFAULT 0,
  `negative_id` smallint(5) unsigned DEFAULT 0,
  `overwrite` smallint(5) unsigned NOT NULL DEFAULT 0,
  `block_id` smallint(5) unsigned DEFAULT 0,
  `remove_id` smallint(5) unsigned NOT NULL DEFAULT 0,
  `element` smallint(5) unsigned NOT NULL DEFAULT 0,
  `min_duration` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `synth_recipes`
--

DROP TABLE IF EXISTS `synth_recipes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `synth_recipes` (
  `ID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Desynth` tinyint(3) unsigned NOT NULL,
  `KeyItem` int(10) unsigned NOT NULL,
  `Alchemy` tinyint(3) unsigned NOT NULL,
  `Bone` tinyint(3) unsigned NOT NULL,
  `Cloth` tinyint(3) unsigned NOT NULL,
  `Cook` tinyint(3) unsigned NOT NULL,
  `Gold` tinyint(3) unsigned NOT NULL,
  `Leather` tinyint(3) unsigned NOT NULL,
  `Smith` tinyint(3) unsigned NOT NULL,
  `Wood` tinyint(3) unsigned NOT NULL,
  `Crystal` smallint(5) unsigned NOT NULL,
  `HQCrystal` smallint(5) unsigned NOT NULL,
  `Ingredient1` smallint(5) unsigned NOT NULL,
  `Ingredient2` smallint(5) unsigned NOT NULL,
  `Ingredient3` smallint(5) unsigned NOT NULL,
  `Ingredient4` smallint(5) unsigned NOT NULL,
  `Ingredient5` smallint(5) unsigned NOT NULL,
  `Ingredient6` smallint(5) unsigned NOT NULL,
  `Ingredient7` smallint(5) unsigned NOT NULL,
  `Ingredient8` smallint(5) unsigned NOT NULL,
  `Result` smallint(5) unsigned NOT NULL,
  `ResultHQ1` smallint(5) unsigned NOT NULL,
  `ResultHQ2` smallint(5) unsigned NOT NULL,
  `ResultHQ3` smallint(5) unsigned NOT NULL,
  `ResultQty` tinyint(2) unsigned NOT NULL,
  `ResultHQ1Qty` tinyint(2) unsigned NOT NULL,
  `ResultHQ2Qty` tinyint(2) unsigned NOT NULL,
  `ResultHQ3Qty` tinyint(2) unsigned NOT NULL,
  `ResultName` tinytext NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5601 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=79;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER ensure_ingredients_are_ordered
     BEFORE INSERT ON synth_recipes FOR EACH ROW BEGIN
          IF NEW.Ingredient2 > 0 AND NEW.Ingredient1 > NEW.Ingredient2
          THEN
            SIGNAL SQLSTATE VALUE '45000'
            SET MESSAGE_TEXT = '[table:synth_recipes] - `Ingredient1` is larger than Ingredient2';
          END IF;

          IF NEW.Ingredient3 > 0 AND NEW.Ingredient2 > NEW.Ingredient3
          THEN
            SIGNAL SQLSTATE VALUE '45000'
            SET MESSAGE_TEXT = '[table:synth_recipes] - `Ingredient2` is larger than Ingredient3';
          END IF;

          IF NEW.Ingredient4 > 0 AND NEW.Ingredient3 > NEW.Ingredient4
          THEN
            SIGNAL SQLSTATE VALUE '45000'
            SET MESSAGE_TEXT = '[table:synth_recipes] - `Ingredient3` is larger than Ingredient4';
          END IF;

          IF NEW.Ingredient5 > 0 AND NEW.Ingredient4 > NEW.Ingredient5
          THEN
            SIGNAL SQLSTATE VALUE '45000'
            SET MESSAGE_TEXT = '[table:synth_recipes] - `Ingredient4` is larger than Ingredient5';
          END IF;

          IF NEW.Ingredient6 > 0 AND NEW.Ingredient5 > NEW.Ingredient6
          THEN
            SIGNAL SQLSTATE VALUE '45000'
            SET MESSAGE_TEXT = '[table:synth_recipes] - `Ingredient5` is larger than Ingredient6';
          END IF;

          IF NEW.Ingredient7 > 0 AND NEW.Ingredient6 > NEW.Ingredient7
          THEN
            SIGNAL SQLSTATE VALUE '45000'
            SET MESSAGE_TEXT = '[table:synth_recipes] - `Ingredient6` is larger than Ingredient7';
          END IF;

          IF NEW.Ingredient8 > 0 AND NEW.Ingredient7 > NEW.Ingredient8
          THEN
            SIGNAL SQLSTATE VALUE '45000'
            SET MESSAGE_TEXT = '[table:synth_recipes] - `Ingredient7` is larger than Ingredient8';
          END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `traits`
--

DROP TABLE IF EXISTS `traits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `traits` (
  `traitid` tinyint(3) unsigned NOT NULL,
  `name` text NOT NULL,
  `job` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `level` tinyint(2) unsigned NOT NULL DEFAULT 99,
  `rank` tinyint(2) unsigned NOT NULL DEFAULT 1,
  `modifier` smallint(5) unsigned NOT NULL DEFAULT 0,
  `value` smallint(5) NOT NULL DEFAULT 0,
  `content_tag` varchar(7) DEFAULT NULL,
  `meritid` smallint(5) NOT NULL DEFAULT 0,
  PRIMARY KEY (`traitid`,`job`,`level`,`rank`,`modifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transport`
--

DROP TABLE IF EXISTS `transport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transport` (
  `id` tinyint(3) unsigned NOT NULL,
  `name` tinytext NOT NULL,
  `transport` int(10) unsigned NOT NULL DEFAULT 0,
  `door` int(10) unsigned NOT NULL DEFAULT 0,
  `dock_x` float(7,3) NOT NULL DEFAULT 0.000,
  `dock_y` float(7,3) NOT NULL DEFAULT 0.000,
  `dock_z` float(7,3) NOT NULL DEFAULT 0.000,
  `dock_rot` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `boundary` smallint(5) unsigned NOT NULL DEFAULT 0,
  `anim_arrive` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `anim_depart` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `time_offset` smallint(5) unsigned NOT NULL DEFAULT 0,
  `time_interval` smallint(5) unsigned NOT NULL DEFAULT 0,
  `time_anim_arrive` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `time_waiting` smallint(5) unsigned NOT NULL DEFAULT 0,
  `time_anim_depart` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `zone` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `water_points`
--

DROP TABLE IF EXISTS `water_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `water_points` (
  `waterid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zoneid` smallint(3) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `pointid` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `pos_x` float(7,2) NOT NULL DEFAULT 0.00,
  `pos_y` float(7,2) NOT NULL DEFAULT 0.00,
  `pos_z` float(7,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`waterid`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `weapon_skills`
--

DROP TABLE IF EXISTS `weapon_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `weapon_skills` (
  `weaponskillid` tinyint(3) unsigned NOT NULL,
  `name` text NOT NULL,
  `jobs` binary(22) NOT NULL,
  `type` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `skilllevel` smallint(3) unsigned NOT NULL DEFAULT 0,
  `element` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `animation` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `animationTime` smallint(4) unsigned NOT NULL DEFAULT 0,
  `range` tinyint(2) unsigned NOT NULL DEFAULT 5,
  `aoe` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `primary_sc` tinyint(4) NOT NULL DEFAULT 0,
  `secondary_sc` tinyint(4) NOT NULL DEFAULT 0,
  `tertiary_sc` tinyint(4) NOT NULL DEFAULT 0,
  `main_only` tinyint(1) NOT NULL DEFAULT 0,
  `unlock_id` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`weaponskillid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `web_tokens`
--

DROP TABLE IF EXISTS `web_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `web_tokens` (
  `account` int(10) NOT NULL,
  `token` varchar(20) NOT NULL,
  `expiration` datetime NOT NULL,
  `type` int(10) NOT NULL,
  PRIMARY KEY (`account`),
  KEY `expiration` (`expiration`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zone_settings`
--

DROP TABLE IF EXISTS `zone_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zone_settings` (
  `zoneid` smallint(3) unsigned NOT NULL DEFAULT 0,
  `zonetype` smallint(5) unsigned NOT NULL DEFAULT 0,
  `zoneip` tinytext NOT NULL,
  `zoneport` smallint(5) unsigned NOT NULL DEFAULT 0,
  `name` tinytext NOT NULL,
  `music_day` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `music_night` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `battlesolo` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `battlemulti` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `restriction` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `tax` float(5,2) unsigned NOT NULL DEFAULT 0.00,
  `misc` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fame_type` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`zoneid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=20 PACK_KEYS=1 CHECKSUM=1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zone_weather`
--

DROP TABLE IF EXISTS `zone_weather`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zone_weather` (
  `zoneid` smallint(3) unsigned NOT NULL DEFAULT 0,
  `weather_day` smallint(4) unsigned NOT NULL DEFAULT 0,
  `common` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `normal` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `rare` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`zoneid`,`weather_day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zonelines`
--

DROP TABLE IF EXISTS `zonelines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zonelines` (
  `zoneline` int(10) unsigned NOT NULL,
  `fromzone` smallint(3) unsigned NOT NULL DEFAULT 0,
  `tozone` smallint(3) unsigned NOT NULL DEFAULT 0,
  `tox` float(7,3) NOT NULL DEFAULT 0.000,
  `toy` float(7,3) NOT NULL DEFAULT 0.000,
  `toz` float(7,3) NOT NULL DEFAULT 0.000,
  `rotation` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`zoneline`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'ffxiwings'
--

--
-- Dumping routines for database 'ffxiwings'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-03 23:40:33
