/*
===========================================================================

Part of the Wings Source Code to work with the Setz0r fishing

===========================================================================
*/

#include "../ai_container.h"
#include "../../entities/charentity.h"
#include "../../packets/action.h"
#include "fishing_state.h"

CFishingState::CFishingState(CCharEntity* PChar) :
    CState(PChar, PChar->targid),
    m_PEntity(PChar)
{

}

bool CFishingState::Update(time_point tick)
{
    if (!IsCompleted() && tick > GetEntryTime() + 1500ms)
    {
        Complete();
        m_PEntity->animation = ANIMATION_NONE;
        m_PEntity->updatemask |= UPDATE_HP;
        return true;
    }

    return false;
}

