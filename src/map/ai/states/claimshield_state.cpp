/*
===========================================================================

Part of the Wings Source Code

===========================================================================
*/

#include "claimshield_state.h"
#include "../ai_container.h"
#include "../../entities/battleentity.h"
#include "../../entities/mobentity.h"
#include "../../status_effect_container.h"
#include "../../utils/battleutils.h"
#include "../../mob_modifier.h"

CClaimShieldState::CClaimShieldState(CBaseEntity* PEntity) :
    CState(PEntity, 0)
{
    
}


bool CClaimShieldState::Update(time_point tick)
{
    if (!IsCompleted() && tick > GetEntryTime() + 7000ms)
    {
        Complete();
        CMobEntity* mob = (CMobEntity*)m_PEntity;
        mob->setMobMod(MOBMOD_CLAIM_SHIELD_ACTIVE, 0);
        battleutils::DoClaimShieldLottery(mob);
        return true;
    }

    return false;
}

void CClaimShieldState::Cleanup(time_point tick)
{}
