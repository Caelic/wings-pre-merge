// part of the wings source code blah blah im not good at licenses or whatever

#include "pruned_session.h"

pruned_session::pruned_session(uint32 accid, uint32 charid, uint32* session_key, uint32 ZoneIP, uint16 ZonePort, uint32 client_addr, uint8 version_mismatch, char client_version[32])
{
    m_0_accid = accid;
    m_1_charid = charid;
    memcpy(m_2_session_key, session_key, sizeof(uint32)*5);
    m_3_ZoneIP = ZoneIP;
    m_4_ZonePort = ZonePort;
    m_5_client_addr = client_addr;
    m_6_version_mismatch = version_mismatch;
    memcpy(m_7_client_version, client_version, sizeof(client_version));

    m_recoveryQueued = false;
}

pruned_session::~pruned_session()
{

}
