@echo off
cls
title Server Auto Restarter
:DSGame-server
echo (%time%) Process started.
start /wait topaz_game_64.exe --ip 127.0.0.1 --port 55777
echo (%time%) WARNING: Process closed or crashed, restarting...
goto DSGame-server