@echo off
echo Stoping all services...
net stop TopazGameCluster10050
net stop TopazGameCluster10200
net stop TopazGameCluster10201
net stop TopazGameCluster10400
net stop TopazGameCluster10500
net stop TopazGameCluster10600
net stop TopazGameCluster10650
net stop TopazGameCluster10750
net stop TopazGameCluster10800
net stop TopazGameCluster10850
net stop TopazGameCluster10900
net stop TopazGameCluster10950
net stop TopazGameCluster11000
net stop TopazGameCluster11050
net stop TopazGameCluster11100
net stop TopazGameCluster11150
net stop TopazGameCluster11300
net stop TopazGameCluster11350
net stop TopazGameCluster11400
net stop TopazGameCluster11500
net stop TopazGameCluster11650
net stop TopazGameCluster11660
net stop TopazGameCluster11670
net stop TopazGameClusterSpecial
net stop TopazGameClusterDebug
net stop TopazSearch
net stop TopazConnect
echo Stoped.
