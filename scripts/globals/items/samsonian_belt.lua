-----------------------------------------
-- ID: 15863
-- Item: samsonian_belt
-- Item Effect: STR +3
-- Duration: 60 seconds
-----------------------------------------


require("scripts/globals/status")

-----------------------------------------

function onItemCheck(target)
    return 0
end

function onItemUse(target)
    target:addStatusEffect(tpz.effect.ENCHANTMENT,0,0,60,15863)
end

function onEffectGain(target,effect)
    target:addMod(tpz.mod.STR, 3)
end

function onEffectLose(target, effect)
    target:delMod(tpz.mod.STR, 3)
end
