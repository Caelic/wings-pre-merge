-----------------------------------
-- Ability: Cover
-- protects target from physical attacks
-- Obtained: Paladin
-- Duration: 15-30
-----------------------------------
require("scripts/globals/settings")
require("scripts/globals/status")
-----------------------------------

function onAbilityCheck(player,target,ability)
    return 0, 0
end

function onUseAbility(player,target,ability)
    local duration = 15 + math.floor((player:getStat(tpz.mod.MND) + player:getStat(tpz.mod.VIT) - target:getStat(tpz.mod.VIT)*2)/4)
    
    if duration < 15 then
        duration = 15
    elseif duration > 30 then
        duration = 30
    end
    
    duration = duration + player:getMerit(tpz.merit.COVER_EFFECT_LENGTH) + player:getMod(tpz.mod.COVER_DURATION)
    
    -- duration boosts go here, after the cap
    
    player:delStatusEffect(tpz.effect.COVER)
    target:delStatusEffect(tpz.effect.COVER)
    
    -- effect powers
    -- 1: base
    -- 3: convert 20% damage taken to MP (valor surcoat) [1+2]
    -- 5: intercept magic (gallant coronet) [1+4]
    -- 7: both 2 and 3 [1+2+4]
    
    local power = 1
    if player:getMod(tpz.mod.ENHANCES_COVER) > 0 then
        power = power + 4
    end
    if player:getMod(tpz.mod.AUGMENTS_COVER) > 0 then
        power = power + 2
    end
    
    player:addStatusEffect(tpz.effect.COVER, power, 3, duration)
    target:addStatusEffect(tpz.effect.COVER, power, 3, duration)

    return 0
end
