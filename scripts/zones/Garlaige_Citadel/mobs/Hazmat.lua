-----------------------------------
-- Area: Garlaige Citadel
--  Mob: Hazmat
-----------------------------------
require("scripts/globals/status")
-----------------------------------

function onMobSpawn(mob)
    mob:setMod(tpz.mod.FASTCAST,50)
    mob:setMod(tpz.mod.REFRESH,20)
end

function onMobDeath(mob, player, isKiller)
end
