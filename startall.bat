@echo off
echo Starting all services...
net start TopazGameCluster10050
net start TopazGameCluster10200
net start TopazGameCluster10201
net start TopazGameCluster10400
net start TopazGameCluster10500
net start TopazGameCluster10600
net start TopazGameCluster10650
net start TopazGameCluster10750
net start TopazGameCluster10800
net start TopazGameCluster10850
net start TopazGameCluster10900
net start TopazGameCluster10950
net start TopazGameCluster11000
net start TopazGameCluster11050
net start TopazGameCluster11100
net start TopazGameCluster11150
net start TopazGameCluster11300
net start TopazGameCluster11350
net start TopazGameCluster11400
net start TopazGameCluster11500
net start TopazGameCluster11650
net start TopazGameCluster11660
net start TopazGameCluster11670
net start TopazGameClusterSpecial
net start TopazGameClusterDebug
net start TopazSearch
net start TopazConnect
echo Started.
